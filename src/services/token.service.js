const jwt = require('jsonwebtoken');
const {
  JWT_ACCESS_SECRET,
  JWT_REFRESH_SECRET,
} = require('../config');
const { TokenModel } = require('../models/token.model');

class TokenService {
  /**
   * Создание новой пары accessToken и refreshToken
   * @param payload - данные, относительно которых будут генерироваться токены
   */
  generateTokens(payload) {
    const accessToken = jwt.sign(payload, JWT_ACCESS_SECRET, { expiresIn: '10m' });
    const refreshToken = jwt.sign(payload, JWT_REFRESH_SECRET, { expiresIn: '3d' });

    return {
      accessToken,
      refreshToken,
    };
  }

  /**
   * Валидация accessToken
   * @param { string } token - валидируемый accessToken
   */
  validateAccessToken(token) {
    try {
      return jwt.verify(token, JWT_ACCESS_SECRET);
    } catch (e) {
      return null;
    }
  }

  /**
   * Валидация refreshToken
   * @param { string } token - валидируемый refreshToken
   */
  validateRefreshToken(token) {
    try {
      return jwt.verify(token, JWT_REFRESH_SECRET);
    } catch (e) {
      return null;
    }
  }

  /**
   * Сохраненение в БД accessToken и refreshToken
   * @param { string } userId - ID пользователя, чьи токены мы сохраняем
   * @param { string } accessToken - сохраняемый accessToken
   * @param { string } refreshToken - сохраняемый refreshToken
   */
  async saveToken(userId, accessToken, refreshToken) {
    await TokenModel.sync();

    const tokenData = await TokenModel.findOne({ where: { userId } });

    if (tokenData) {
      await TokenModel.update({ refreshToken }, {
        where: { userId },
      });
    }

    return TokenModel.create({ userId, accessToken, refreshToken });
  }

  /**
   * Удаление данных токенов из БД
   * @param { string } refreshToken - refreshToken, по которому мы удаляем токены
   */
  async removeToken(refreshToken) {
    await TokenModel.sync();

    return TokenModel.update({ accessToken: null, refreshToken: null }, {
      where: { refreshToken },
    });
  }

  /**
   * Поиск токенов по refreshToken
   * @param { string } refreshToken - refreshToken, по которому мы ищем токены
   */
  async findToken(refreshToken) {
    await TokenModel.sync();

    return TokenModel.findOne({ where: { refreshToken } });
  }
}

module.exports = new TokenService();
