const path = require('path');
const fs = require('fs');
const { FileModel } = require('../models/file.model');
const ApiError = require('../exceptions/api.error');

class FileService {
  /**
   * Добавление нового файла в систему и запись параметров файла в базу: название, расширение, MIME type, размер, дата загрузки
   * @param file - загружаемый файл
   */
  async upload(file) {
    await FileModel.sync();

    return FileModel.create({
      name: file.originalname,
      dbName: file.filename,
      extension: path.extname(file.originalname),
      mimeType: file.mimetype,
      size: file.size,
      uploadDate: new Date(),
    });
  }

  /**
   * Выводит список файлов и их параметров из базы с использованием пагинации с размером страницы
   * @param { number } list_size - размер страницы (по умолчанию 10)
   * @param { number } page - номер страницы (по умолчанию 1)
   */
  async list({ list_size = 10, page = 1 }) {
    await FileModel.sync();

    return FileModel.findAll({
      limit: parseInt(list_size, 10),
      offset: parseInt(list_size * (page - 1), 10),
    });
  }

  /**
   * Удаляет документ из базы и локального хранилища
   * @param { number } id - ID удаляемого документа
   */
  async deleteById(id) {
    const fileToDelete = await this.getById(id);

    if (!fileToDelete) {
      throw ApiError.BadRequest(`Файл c ID ${id} не найден в базе данных`);
    }

    try {
      await fs.unlink(`./uploads/${fileToDelete.dbName}`, () => {});
    } catch (e) {
      throw ApiError.BadRequest('Файл отсутствует на диске');
    }

    return FileModel.destroy({ where: { id } });
  }

  /**
   * Вывод информации о выбранном файле
   * @param { number } id - ID выбранного документа
   */
  async getById(id) {
    await FileModel.sync();

    return FileModel.findOne({ where: { id } });
  }

  /**
   * Скачивание конкретного файла
   * @param { number } id - ID скачиваемого документа
   */
  async downloadById(id) {
    await FileModel.sync();

    const fileToDownload = await this.getById(id);

    if (!fileToDownload) {
      throw ApiError.BadRequest('Файл не найден в базе данных');
    }

    return {
      fileData: `./uploads/${fileToDownload.dbName}`,
      fileName: fileToDownload.name,
    };
  }

  /**
   * Обновление текущего документа на новый в базе и локальном хранилище
   * @param { number } id - ID обновляемого документа
   * @param file - обновляемый файл
   */
  async updateById(id, file) {
    await FileModel.sync();

    const fileToUpdate = await this.getById(id);

    if (!fileToUpdate) {
      throw ApiError.BadRequest(`Файл c ID ${id} не найден в базе данных`);
    }

    try {
      await fs.unlink(`./uploads/${fileToUpdate.dbName}`, () => {});
    } catch (e) {
      throw ApiError.BadRequest('Файл отсутствует на диске');
    }

    return FileModel.update({
      name: file.originalname,
      dbName: file.filename,
      extension: path.extname(file.originalname),
      mimeType: file.mimetype,
      size: file.size,
      uploadDate: new Date(),
    }, {
      where: { id },
    });
  }
}

module.exports = new FileService();
