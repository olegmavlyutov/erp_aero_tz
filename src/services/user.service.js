const bcrypt = require('bcrypt');
const tokenService = require('./token.service');
const { UserModel } = require('../models/user.model');
const { TokenModel } = require('../models/token.model');
const ApiError = require('../exceptions/api.error');

class UserService {
  /**
   * Регистрация нового пользователя
   * @param { string } id - номер телефона или email пользователя
   * @param { string } password - пароль пользователя
   */
  async signUp(id, password) {
    await UserModel.sync();

    const candidate = await UserModel.findOne({ where: { id } });

    if (candidate) {
      throw ApiError.BadRequest(`Пользователь ${id} уже существует`);
    }

    const hashedPassword = await bcrypt.hash(password, 3);
    const user = await UserModel.create({ id, password: hashedPassword });

    const tokens = tokenService.generateTokens({ id: user.id });
    await tokenService.saveToken(user.id, tokens.accessToken, tokens.refreshToken);

    return {
      ...tokens,
      userId: user.id,
    };
  }

  /**
   * Запрос Bearer токена по id и паролю (авторизация)
   * @param { string } id - номер телефона или email пользователя
   * @param { string } password - пароль пользователя
   */
  async signIn(id, password) {
    await UserModel.sync();

    const user = await UserModel.findOne({ where: { id } });

    if (!user) {
      throw ApiError.BadRequest(`Пользователь ${id} не найден`);
    }

    const isPassEqual = await bcrypt.compare(password, user.password);

    if (!isPassEqual) {
      throw ApiError.BadRequest('Неверный пароль');
    }

    const tokens = tokenService.generateTokens({ id: user.id });
    await tokenService.saveToken(user.id, tokens.accessToken, tokens.refreshToken);

    return {
      ...tokens,
      userId: user.id,
    };
  }

  /**
   * Выйти из системы
   * @param { string } refreshToken - refresh токен
   */
  async logout(refreshToken) {
    return tokenService.removeToken(refreshToken);
  }

  /**
   * Обновление Bearer токена по refresh токену
   * @param { string } refreshToken - refresh токен
   */
  async newToken(refreshToken) {
    await UserModel.sync();

    if (!refreshToken) {
      throw ApiError.UnauthorizedError();
    }

    const userData = tokenService.validateRefreshToken(refreshToken);
    const tokenFromDB = await tokenService.findToken(refreshToken);

    if (!userData && !tokenFromDB) {
      throw ApiError.UnauthorizedError();
    }

    const user = await UserModel.findOne({ wher: { id: userData.id } });
    const tokens = tokenService.generateTokens({ id: user.id });

    await tokenService.saveToken(user.id, tokens.refreshToken);

    return {
      ...tokens,
      userId: user.id,
    };
  }

  /**
   * Возвращает id пользователя
   * @param { string } refreshToken - refresh токен
   */
  async info(refreshToken) {
    await TokenModel.sync();

    if (!refreshToken) {
      throw ApiError.UnauthorizedError();
    }

    const tokens = await TokenModel.findOne({ where: { refreshToken } });

    return tokens.userId;
  }
}

module.exports = new UserService();
