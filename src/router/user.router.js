const { Router } = require('express');
const { body } = require('express-validator');
const userController = require('../controllers/user.controller');
const authMiddleware = require('../middlewares/auth.middleware');

const userRouter = new Router();

userRouter.post(
  '/signin',
  body('id')
    .isEmail()
    .isLength({ min: 1 }),
  body('password')
    .isLength({ min: 1 }),
  userController.signIn,
);
userRouter.post('/signin/new_token', authMiddleware, userController.newToken);
userRouter.post('/signup', userController.signUp);

userRouter.get('/info', authMiddleware, userController.info);
userRouter.get('/logout', authMiddleware, userController.logout);

module.exports = userRouter;
