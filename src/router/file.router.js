const { Router } = require('express');
const upload = require('../middlewares/upload.middleware');
const fileController = require('../controllers/file.controller');
const authMiddleware = require('../middlewares/auth.middleware');

const fileRouter = new Router();

fileRouter.post('/upload', authMiddleware, upload.single('fileData'), fileController.upload);
fileRouter.get('/list', authMiddleware, fileController.list);
fileRouter.delete('/delete/:id', authMiddleware, fileController.deleteById);
fileRouter.get('/:id', authMiddleware, fileController.getById);
fileRouter.get('/download/:id', authMiddleware, fileController.downloadById);
fileRouter.put('/update/:id', authMiddleware, upload.single('fileData'), fileController.updateById);

module.exports = fileRouter;
