const fileService = require('../services/file.service');

class FileController {
  async upload(req, res, next) {
    try {
      const uploadedFile = await fileService.upload(req.file);

      await res.status(201).json(uploadedFile);
    } catch (e) {
      next(e);
    }
  }

  async list(req, res, next) {
    try {
      const { list_size, page } = req.query;

      const fileList = await fileService.list({ list_size, page });
      await res.json(fileList);
    } catch (e) {
      next(e);
    }
  }

  async deleteById(req, res, next) {
    try {
      const { id } = req.params;

      const deletedFile = await fileService.deleteById(id);

      await res.json(deletedFile);
    } catch (e) {
      next(e);
    }
  }

  async getById(req, res, next) {
    try {
      const { id } = req.params;

      const fileInfo = await fileService.getById(id);

      await res.json(fileInfo);
    } catch (e) {
      next(e);
    }
  }

  async downloadById(req, res, next) {
    try {
      const { id } = req.params;

      const { fileData, fileName } = await fileService.downloadById(id);

      await res.download(fileData, fileName);
    } catch (e) {
      next(e);
    }
  }

  async updateById(req, res, next) {
    try {
      const { id } = req.params;

      const updatedFile = await fileService.updateById(id, req.file);

      await res.json(updatedFile);
    } catch (e) {
      next(e);
    }
  }
}

module.exports = new FileController();
