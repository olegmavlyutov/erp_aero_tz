const { validationResult } = require('express-validator');
const userService = require('../services/user.service');
const ApiError = require('../exceptions/api.error');

class UserController {
  async signUp(req, res, next) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return next(ApiError.BadRequest(`Ошибка валидации ${errors.array()}`));
      }

      const { id, password } = req.body;
      const userData = await userService.signUp(id, password);

      res.cookie('refreshToken', userData.refreshToken, { maxAge: 3 * 24 * 60 * 60 * 1000, httpOnly: true });

      await res.json(userData);
    } catch (e) {
      next(e);
    }
  }

  async signIn(req, res, next) {
    try {
      const { id, password } = req.body;
      const userData = await userService.signIn(id, password);

      res.cookie('refreshToken', userData.refreshToken, { maxAge: 3 * 24 * 60 * 60 * 1000, httpOnly: true });

      // на самом деле Bearer можно хранить как в httpOnly куках так и перехватывать в axios interceptors
      res.cookie('accessToken', `Bearer ${userData.accessToken}`, { maxAge: 10 * 60 * 1000, httpOnly: true });
      res.setHeader('Authorization', `Bearer ${userData.accessToken}`);

      await res.json(userData);
    } catch (e) {
      next(e);
    }
  }

  async newToken(req, res, next) {
    try {
      const { refreshToken } = req.cookies;
      const userData = await userService.newToken(refreshToken);

      res.cookie('refreshToken', userData.refreshToken, { maxAge: 3 * 24 * 60 * 60 * 1000, httpOnly: true });
      res.cookie('accessToken', `Bearer ${userData.accessToken}`, { maxAge: 10 * 60 * 1000, httpOnly: true });
      res.setHeader('Authorization', `Bearer ${userData.accessToken}`);

      await res.json(userData);
    } catch (e) {
      next(e);
    }
  }

  async info(req, res, next) {
    try {
      const { refreshToken } = req.cookies;
      const info = await userService.info(refreshToken);

      await res.json(info);
    } catch (e) {
      next(e);
    }
  }

  async logout(req, res, next) {
    try {
      const { refreshToken } = req.cookies;
      await userService.logout(refreshToken);
      res.clearCookie('refreshToken');
      res.clearCookie('accessToken');

      return res.json('Вы успешно вышли из системы');
    } catch (e) {
      next(e);
    }
  }
}

module.exports = new UserController();
