const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const { connect } = require('./sequelizeConnect');
const { HTTP_PORT } = require('./config');
const userRouter = require('./router/user.router');
const fileRouter = require('./router/file.router');
const errorMiddleware = require('./middlewares/error.middleware');

const app = express();

app.use(express.json());
app.use(cookieParser());
app.use(cors());
app.use('/', userRouter);
app.use('/file', fileRouter);
app.use(errorMiddleware);

const startServer = async () => {
  try {
    await connect();
    app.listen(HTTP_PORT, () => console.log(`Server successfully started on port ${HTTP_PORT}`));
  } catch (e) {
    console.log(e);
  }
};

startServer()
  .catch((e) => {
    console.log(e);

    process.exit(1);
  });
