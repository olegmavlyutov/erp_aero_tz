const { Sequelize } = require('sequelize');
const {
  DB_NAME,
  DB_USERNAME,
  DB_PASSWORD,
} = require('./config');

const sequelize = new Sequelize({
  database: DB_NAME,
  username: DB_USERNAME,
  password: DB_PASSWORD,
  dialect: 'mysql',
  logging: true,
});

async function connect() {
  try {
    await sequelize.authenticate();
    console.log('Connection to DB has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
}

if (!module.parent) {
  connect()
    .catch((e) => {
      console.log(e);

      process.exit(1);
    });
}

module.exports = {
  sequelize,
  connect,
};
