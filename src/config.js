require('dotenv').config();

const {
  HTTP_PORT = 8000,

  DB_NAME = 'aero_db',
  DB_USERNAME = 'root',
  DB_PASSWORD = 'password',

  JWT_ACCESS_SECRET = 'jwt-access-secret',
  JWT_REFRESH_SECRET = 'jwt-refresh-secret',
} = process.env;

module.exports = {
  HTTP_PORT,

  DB_NAME,
  DB_USERNAME,
  DB_PASSWORD,

  JWT_ACCESS_SECRET,
  JWT_REFRESH_SECRET,
};
