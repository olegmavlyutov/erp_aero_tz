const { DataTypes } = require('sequelize');
const { sequelize } = require('../sequelizeConnect');

const FileModel = sequelize.define('FileModel', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  name: DataTypes.STRING,

  // добавил это поле, чтобы знать какой из файлов удалять из локального хранилища, т.к. переименовываю их при загрузке
  dbName: DataTypes.STRING,

  extension: DataTypes.STRING,
  mimeType: DataTypes.STRING,
  size: DataTypes.STRING,
  uploadDate: DataTypes.DATE,
}, {
  timestamps: false,
});

module.exports = {
  FileModel,
};
