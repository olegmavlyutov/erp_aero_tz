const { DataTypes } = require('sequelize');
const { sequelize } = require('../sequelizeConnect');

const UserModel = sequelize.define('UserModel', {
  id: {
    type: DataTypes.STRING,
    primaryKey: true,
  },
  password: DataTypes.STRING,
}, {
  timestamps: false,
});

module.exports = {
  UserModel,
};
