const { DataTypes } = require('sequelize');
const { sequelize } = require('../sequelizeConnect');

const TokenModel = sequelize.define('TokenModel', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },

  accessToken: DataTypes.STRING,
  refreshToken: DataTypes.STRING,

  userId: DataTypes.STRING,
}, {
  timestamps: false,
});

module.exports = {
  TokenModel,
};
